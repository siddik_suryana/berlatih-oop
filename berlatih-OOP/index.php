<?php

require_once('animal.php');
$animal = new Animal("shaun");
echo "Name          : ". $animal->name."<br>"; // "shaun"
echo "Legs          : ". $animal->legs."<br>"; // 4
echo "Cold Blooded  : ". $animal->cold_blooded."<br>"."<br>";// "no"

$animal = new Animal("buduk");
echo "Name         : ". $animal->name."<br>"; // "shaun"
echo "Legs         : ". $animal->legs."<br>"; // 4
echo "Cold Blooded : ". $animal->cold_blooded."<br>";
echo "Jump         : ".$animal->jump."<br>"."<br>";

$animal = new Animal("Kera Sakti");
echo "Name         : ". $animal->name."<br>"; // "shaun"
echo "Legs         : ". $animal->legs."<br>"; // 4
echo "Cold Blooded : ". $animal->cold_blooded."<br>";
echo "Yell         : ". $animal->yell."<br>"."<br>";
?>